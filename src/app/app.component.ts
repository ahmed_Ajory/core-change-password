import { Component, isDevMode } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Router } from "@angular/router";
// import { CustomTranslateService } from "../translate/custom-translate.service";
import { TranslateService } from "@ngx-translate/core";
import { environment } from "src/environments/environment";

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.scss"]
})
export class AppComponent {
  title = "core-change-password";
  user: any;
  accessToken: any;
  activeFieldId: string = "";
  service_url = environment.service_url;
  login_url = environment.login_url;
  translate;
  username;
  constructor(translate: TranslateService, private http: HttpClient) {
    // this language will be used as a fallback when a translation isn't found in the current language
    translate.setDefaultLang("ar");

    // the lang to use, if the lang isn't available, it will use the current loader to get them
    // translate.use("ar");
  }

  ngOnInit() {
    this.getToken();
    this.username = this.user.username || "Hesham";
  }

  getToken() {
    if (isDevMode()) {
      document.cookie =
        "kindix-token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VybmFtZSI6Im5hZGVyMTAwIiwidXNlcklkIjoxLCJzdWJEb21haW4iOiJhcHAiLCJyb2xlIjoyLCJpYXQiOjE1NjQ4NTk0MzIsImV4cCI6MTU2NDg5NTQzMn0.aSbx4bMwS54eiCPfO2NqDl58WriEBODn1wLBOgXm_qc";
    }
    let cookie = decodeURIComponent(this.getCookie("kindix-token"));
    if (cookie == "undefined" || cookie == "") {
      window.location.href = this.login_url;
    } else {
      let userCookie = isDevMode()
        ? JSON.parse(JSON.stringify(cookie.trim()))
        : JSON.parse(cookie);
      this.user = userCookie;

      if (isDevMode()) {
        this.user = { accessToken: userCookie };
        return cookie;
      } else {
        if (this.user.lang == "ar") {
          this.changeLang("ar");
        } else {
          this.changeLang("he");
        }
      }
      return userCookie.accessToken;
    }
  }

  changeLang(lang) {
    localStorage.removeItem("core_lang");
    localStorage.setItem("core_lang", JSON.stringify({ lang: lang }));
    this.translate.use(lang);
  }

  onLogout() {
    this.getToken();
    document.cookie = "kindix-token=;";
    window.location.href = this.user.logout_url;
  }

  getCookie(name) {
    var value = "; " + document.cookie;
    var parts = value.split("; " + name + "=");
    if (parts.length == 2)
      return parts
        .pop()
        .split(";")
        .shift();
  }

  onFocus(fieldId): void {
    this.activeFieldId = fieldId;
  }
  onFocusout(fieldId): void {
    this.activeFieldId = fieldId;
  }

  changePassword(accessToken: any, params: {}) {
    const headers = new HttpHeaders({
      "Content-Type": "application/json",
      authorization: accessToken
    });
    return this.http.post(
      this.service_url +
        "accounts/change_password" +
        "?authorization=" +
        accessToken,
      JSON.stringify(params),
      { headers: headers }
    );
  }
  onSubmit(form) {
    form.value["old_username"] = this.username;
    console.log(this.accessToken);
    this.changePassword(this.accessToken, form.value).subscribe(
      res => {
        console.log(res);
        window.location.href = this.login_url;
      },
      err => {
        console.log({ err });
      }
    );
    console.log(form.value);
  }
}
