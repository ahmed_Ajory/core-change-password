export const environment = {
  production: true,
  service_url: "http://core-api.kindix.me/api/",
  login_url: "https://authedu.kindix.me/?appcode=core"
};
